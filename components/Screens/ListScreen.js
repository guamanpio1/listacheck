import React, { useEffect, useState } from "react";
import Icon from "react-native-vector-icons/FontAwesome5";
import firebase from "../firebase/firebase";
import "firebase/firestore";
import { YellowBox } from 'react-native';
import _ from 'lodash';
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  FlatList,
  ScrollView,
  SnapshotViewIOSBase,
  ActivityIndicator
} from "react-native";
YellowBox.ignoreWarnings(['Setting a timer']);
const _console = _.clone(console);
console.warn = message => {
  if (message.indexOf('Setting a timer') <= -1) {
    _console.warn(message);
  }
};

export default function List({ navigation }) {

  const [data, setData] = useState([]);
  const [isLoading, setLoading] = useState(true);
  const datos = () => {
    let a = [];
    firebase
      .firestore()
      .collection("Tarea")
      .get()
      .then((snapshot) => {
        snapshot.forEach((doc) => {
          a.push(doc.data());
        });
        setData(a);
      })
      .catch((err) => {
        console.log("Error getting documents", err);
      }).finally(() => setLoading(false));
  };

  useEffect(() => {
    let isMounted = true;
    datos();

    return () => {
      isMounted = false;
    };
  }, []);

  return (
    <View style={styles.container}>
      <View style={{ flexDirection: "row" }}>
        <Text
          style={{
            flex: 8,
            padding: 10,
            fontSize: 20,
            fontWeight: "bold",
            borderWidth: 1,
          }}
        >
          <Icon name="arrow-down" size={25} />
          Tareas
        </Text>
        <Text
          style={{
            flex: 3,
            padding: 10,
            fontSize: 20,
            fontWeight: "bold",
            borderWidth: 1,
          }}
        >
          Progreso
        </Text>
      </View>
      <View style={{ flex: 10 }}>
      {isLoading ? (<ActivityIndicator size="large" style={styles.containerLoading}/>)
                : (
        <FlatList
          data={data}

          renderItem={({ item }) => (
            <TouchableOpacity onPress={() => navigation.navigate("Tarea",{item})}>
              <View style={{ flexDirection: "row", flex: 1 }}>
                <Text style={{ flex: 10, padding: 10, fontSize: 18 }}>
                  {item.nombre}
                </Text>
              </View>
            </TouchableOpacity>
          )}
          ItemSeparatorComponent={(props) => {
            return (
              <View
                style={{
                  height: 2,
                  width: "100%",
                  backgroundColor: "#000",
                }}
              />
            );
          }}
          style={styles.item}
        />)}
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: { flex: 5, backgroundColor: "white" },

  containerTitle: {
    flex: 1,
    flexDirection: "row",
    alignItems: "center",
    alignContent: "center",
    justifyContent: "center",
    backgroundColor: "#7DD4DD",
  },

  TextTitle: {
    fontSize: 20,
    fontWeight: "bold",
  },
  item: {
    flex: 1,
    borderWidth: 1,
  },

  containerLoading:{
    flex: 1,
    justifyContent: 'center'
  }
});
