import React, { useEffect, useState } from "react";
import {
  View,
  Text,
  TouchableOpacity,
  TextInput,
  StyleSheet,
  CheckBox,
  Alert,
} from "react-native";
import firebase from "../firebase/firebase";
import "firebase/firestore";

export default function Login({ navigation }) {
  const [data, setData] = useState([]);
  const [correo, setEmail] = useState("");
  const [contraseña, setPassword] = useState("");
  const oneDocument = () => {
    firebase
      .firestore()
      .collection("login")
      .where("email", "==", correo)
      .get()
      .then((snapshot) => {
        snapshot.forEach((data) => {
          const item = data.data();
          if (item.contraseña === contraseña) {
            navigation.navigate("Lista de Chequeo");
          } else {
            Alert.alert("contraseña incorrecta");
          }
        });
      })
      .catch((err) => {
        console.log("Error getting documents", err);
      });
  };

  useEffect(() => {
    let isMounted = true;
    oneDocument();

    return () => {
      isMounted = false;
    };
  }, []);

  return (
    <View style={styles.container}>
      <Text style={styles.text}>Checking</Text>

      <TextInput
        style={styles.input}
        underlineColorAndroid="transparent"
        placeholder="Email"
        placeholderTextColor="black"
        autoCapitalize="none"
        onChangeText={(email) => setEmail(email)}
      />
      <Text></Text>
      <TextInput
        style={styles.input}
        underlineColorAndroid="transparent"
        placeholder="Password"
        placeholderTextColor="black"
        autoCapitalize="none"
        onChangeText={(Password) => setPassword(Password)}
      />
      <View
        style={{
          flexDirection: "row",
          alignContent: "center",
          alignItems: "center",
        }}
      >
        <CheckBox style={styles.check} />
        <Text>Recuerdame</Text>
      </View>
      <View style={{ flex: 1, flexDirection: "row-reverse" }}>
        <TouchableOpacity
          style={styles.submitButton}
          onPress={() => oneDocument()}
        >
          <Text style={styles.submitButtonText}> Iniciar sesión </Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
    justifyContent: "center",
    alignContent: "center",
  },
  input: {
    margin: 15,
    height: 40,
    borderBottomColor: "#7DD4DD",
    borderBottomWidth: 2,
    marginBottom: 30,
  },
  submitButton: {
    backgroundColor: "#7DD4DD",
    padding: 10,
    margin: 15,
    height: 40,
    width: 120,
  },
  submitButtonText: {
    color: "black",
  },
  text: {
    fontSize: 60,
    textAlign: "center",
  },
  check: {
    padding: 10,
  },
});
