import React, { useEffect, useState } from "react";
import { useRoute } from "@react-navigation/native";
import Icon from "react-native-vector-icons/FontAwesome5";
import SafeAreaAndroid from "../../GlobalStyle/SafeAreaAndroid";
import firebase from "../firebase/firebase";
import "firebase/firestore";
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  TextInput,
  StatusBar,
  FlatList,
  Dimensions,
  CheckBox,
  ScrollView,
  Button,
  Alert,
  Image,
  ImageBackground,
  ActivityIndicator,

} from "react-native";
import {
  Table,
  TableWrapper,
  Row,
  Rows,
  Col,
} from "react-native-table-component";


const createThreeButtonAlert = () =>
  Alert.alert(
    "Confirmacion",
    "Las tareas se han enviado correctamente",
    [{ text: "OK" }],
    { cancelable: false }
  );
export default function ListWork() {
  let route = useRoute();
  const [check, setCheck] = useState(false)
  const [isLoading, setLoading] = useState(true);
  const { item } = route.params;
  const [admin, setData] = useState([]);
  const [cumplido, setTarea] = useState("");
  const oneDocument = () => {
    const a = [];
    firebase
      .firestore()
      .collection("administrador")
      .where("ref", "==", item.id)
      .get()
      .then((snapshot) => {
        snapshot.forEach((doc) => {
          a.push(doc.data());
        });
        setData(a);
      })
      .catch((err) => {
        console.log("Error getting documents", err);
      }).finally(() => setLoading(false));
    /*         firebase.firestore().collection('Tarea').doc(item.id).collection('tCumplidos').add({
          cumplido: cumplido

        }).then(()=>{
          console.log('enviado')
        })        
        .catch((err) => {
          console.log('Error getting documents', err);
      }); 
*/
  };

  const workDocument = () => {
    let a = [];
    firebase
      .firestore()
      .collection("Tarea")
      .doc(item.id)
      .collection("tCumplidos")
      .get()
      .then((snapshot) => {
        snapshot.forEach((data) => {
          a.push(data.data());
        });
        setTarea(a);
      })
      .catch((err) => {
        console.log("Error getting documents", err);
      });
  };

  useEffect(() => {
    let isMounted = true;
    oneDocument();
    workDocument();
    return () => {
      isMounted = false;
    };
  }, []);

  return (
    <View style={styles.container}>
      <ScrollView>
        <View style={{ flex: 15 }}>
          <Table borderStyle={{ borderWidth: 1 }}>
            <TableWrapper style={styles.wrapper}>
              <Col
                data={[item.nombre, item.descripcion]}
                style={styles.title}
                heightArr={[20, 70]}
                textStyle={styles.text}
              />
            </TableWrapper>
          </Table>
        </View>

        <View style={styles.containerTitle}>
          <Text style={styles.TextTitle}>Verificacciòn del avance</Text>
        </View>
      </ScrollView>
      <View style={{ flex: 30 }}>
      {isLoading ? (<ActivityIndicator size="large" style={styles.containerLoading}/>)
                : (
        <FlatList
          data={cumplido}
          renderItem={({ item }) => (
            <View style={{ flexDirection: "row" }}>
              <View style={{
                padding:15,
                  height: 50,
                  width: 50,
                  borderRightWidth: 1,
                  backgroundColor:'#7DD4DD'
                }}>
              <Image style={{
                  height: '100%',
                  width: '100%',

                }}               
                source ={{uri: item.icono}}/>
              </View>
              <Text style={{ padding: 10, fontSize: 18, width: "100%" }}>
                {item.cumplido}
              </Text>
            </View>
          )}
          ItemSeparatorComponent={(props) => {
            return (
              <View
                style={{
                  height: 2,
                  width: "100%",
                  backgroundColor: "#000",
                }}
              />
            );
          }}
          style={styles.item}
        />)}
      </View>
      <ScrollView>
        <View style={styles.containerTitle}>
          <Text style={styles.TextTitle}>Lista de avances</Text>
        </View>
      </ScrollView>
      <View
        style={{
          flex: 23,
          alignContent: "center",
          justifyContent: "center",
          flexDirection: "column",
          alignItems: "stretch",
        }}
      >
        {isLoading ? (<ActivityIndicator size="large" style={styles.containerLoading}/>)
                : (
        <FlatList

          data={admin}

          renderItem={({ item }) => (
            <View style={{ flexDirection: "row" }}>
              <View style={{ flex: 5 }}>
                <Text style={{ padding: 10, fontSize: 18 }}>
                  {item.cumplido}
                </Text>
              </View>
              <View>
                <Text style={{ flex: 1, top: 6 }}>
                  <CheckBox title="Click Here" style={styles.check} 
                    iconRight={false}
                    onIconPress={() => {
                        setCheck(!check);
                    }}
                    checked={check}/>
                </Text>
              </View>
            </View>
          )}
          ItemSeparatorComponent={(props) => {
            return (
              <View
                style={{
                  height: 2,
                  width: "100%",
                  backgroundColor: "#000",
                }}
              />
            );
          }}
          style={styles.item}
          nestedScrollEnabled
        />)}
      </View>
      <ScrollView>
        <View style={{ alignItems: "center", justifyContent: "center" }}>
          <TouchableOpacity
            style={styles.submitButton}
            onPress={createThreeButtonAlert}
          >
            <Text style={styles.submitButtonText}> Enviar </Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  container: { flex: 5, backgroundColor: "white" },
  head: { height: 40, backgroundColor: "#f1f8ff" },
  wrapper: { flexDirection: "row" },
  ColumnTask: { flex: 2, backgroundColor: "#f6f8fa" },
  row: { height: 28 },
  text: { textAlign: "center",
  fontSize:20 },
  containerTitle: {
    flex: 3,
    flexDirection: "row",
    alignItems: "center",
    alignContent: "center",
    justifyContent: "center",
    backgroundColor: "#7DD4DD",
  },
  TextTitle: {
    fontSize: 20,
    width: "100%",
    textAlign: "center",
  },
  item: {
    flex: 1,
    borderWidth: 1,
  },
  submitButton: {
    backgroundColor: "#7DD4DD",
    padding: 10,
    margin: 15,
    height: 40,
    width: 120,
  },
  submitButtonText: {
    color: "black",
    textAlign: "center",
  },
  containerLoading:{
    flex: 1,
    justifyContent: 'center'
  },

});
