import React, { Component } from 'react'
import Login from '../Screens/LoginScreen'
import List from '../Screens/ListScreen'
import ListWork from '../Screens/ListWorkScreen'
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

const Stack = createStackNavigator();
function Navigation() {
  return (
    <NavigationContainer>
      <Stack.Navigator>
      <Stack.Screen name="Login" headerStyle= { {backgroundColor: 'red'} } component={Login} options={{
          headerTitleAlign: 'center',
          headerStyle: {
            backgroundColor: '#7DD4DD',
          },
          headerTintColor: '#000',
          headerTitleStyle: {
            fontWeight: 'bold'     
          },
        }} />
        <Stack.Screen name="Lista de Chequeo" component={List} options={{
          headerTitleAlign: 'center',
          headerStyle: {
            backgroundColor: '#7DD4DD',
          },
          headerTintColor: '#000',
          headerLeftContainerStyle: 50,
          headerTitleStyle: {
            fontWeight: 'bold',
          },
        }} />
          <Stack.Screen name="Tarea" component={ListWork} options={{
          headerTitleAlign: 'center',
          headerStyle: {
            backgroundColor: '#7DD4DD',
          },
          headerTintColor: '#000',
          headerLeftContainerStyle: 50,
          headerTitleStyle: {
            fontWeight: 'bold',
          },
        }} />
      </Stack.Navigator>
    </NavigationContainer>
  );
  }
export default Navigation