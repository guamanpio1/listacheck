import * as firebase from "firebase/app";

const firebaseConfig = {
  apiKey: "AIzaSyBtO3UakWMzqpxo0YWGw00F5DykP4AhROA",
  authDomain: "listacheck-34e47.firebaseapp.com",
  databaseURL: "https://listacheck-34e47.firebaseio.com",
  projectId: "listacheck-34e47",
  storageBucket: "listacheck-34e47.appspot.com",
  messagingSenderId: "1061119307939",
  appId: "1:1061119307939:web:5260c5e6ea8789dd354f16",
  measurementId: "G-137VBESZGM"
  };
  export default firebase.initializeApp(firebaseConfig);