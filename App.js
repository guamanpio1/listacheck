import React, { Component } from "react";
import { View } from "react-native";
import NavigatorScreens from "./components/stacks/stackNavigate";
export default function App() {
  return (
    <View style={{ flex: 1 }}>
      <NavigatorScreens />
    </View>
  );
}


